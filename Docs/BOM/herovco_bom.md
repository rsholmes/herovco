# herovco.kicad_sch BOM

Fri 29 Mar 2024 06:42:22 PM EDT

Generated from schematic by Eeschema 7.0.11-7.0.11~ubuntu22.04.1

**Component Count:** 160


## 
| Refs | Qty | Component | Description | Manufacturer | Part | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- | ---- |
| B1–3 | 3 | Board | Board marker annotation for KiKit |  |  |  |  |
| C1–6, C16, C17 | 8 | 100nF | Ceramic capacitor, pitch 2.5 mm |  |  | Tayda |  |
| C7, C12 | 2 | 100nF | Film capacitor, pitch 5 mm |  |  | Tayda | A-564 |
| C8, C10, C11, C13 | 4 | 10nF | Film capacitor, pitch 5 mm |  |  | Tayda | A-559 |
| C9 | 1 | 1nF | Unpolarized capacitor |  |  | Digi-Key | FKP2G011001D00HSSD |
| C14, C15 | 2 | 10uF | Electrolytic capacitor, pitch 2.5 mm |  |  | Tayda |  |
| C18, C19 | 2 | 330pF | Ceramic capacitor, pitch 5 mm |  |  | Tayda |  |
| D1, D2 | 2 | 1N4148 | Standard switching diode, DO-35 |  |  | Tayda | A-157 |
| D3, D4 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 |  |  | Tayda | A-159 |
| GRAF1 | 1 | Holes | Graphic |  |  |  |  |
| GRAF2 | 1 | Holes | Graphic |  |  |  |  |
| GRAF3 | 1 | AO logo | Graphic |  |  |  |  |
| GRAF4 | 1 | CC0 logo | Graphic |  |  |  |  |
| H1–6 | 6 | MountingHole | Mounting hole |  |  |  |  |
| J1 | 1 | Conn_01x09 | 1x9 pin header, pitch 2.54 mm |  |  |  |  |
| J2, J3 | 2 | Conn_01x03 | 1x3 pin header, pitch 2.54 mm |  |  |  |  |
| J4 | 1 | 3_pin_Molex_header | KK254 Molex header |  |  | Tayda | A-805 |
| J5, J13 | 2 | Wire pads | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |  |  |
| J6–8, J14–16 | 6 | Conn_01x01 | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |  |  |
| J9 | 1 | Conn_01x09 | 1x9 pin socket, pitch 2.54 mm |  |  |  |  |
| J10, J11 | 2 | Conn_01x03 | 1x3 pin socket, pitch 2.54 mm |  |  |  |  |
| J12 | 1 | 3_pin_Molex_connector | KK254 Molex connector |  |  | Tayda | A-827 |
| J17 | 1 | Conn_02x04_Odd_Even | 2x4 shrouded pin header, pitch 2.54 mm |  |  |  |  |
| J18–22, J24–28 | 10 | AudioJack2 | 1/4" audio jack, vertical |  |  | Tayda | A-1121 |
| J23 | 1 | Synth_power_2x5 | 2x5 shrouded pin header, pitch 2.54 mm |  |  | Tayda | A-2939 |
| Q1, Q2 | 2 | 2N3904 | Small Signal NPN Transistor, TO-92 |  |  | Tayda | A-111 |
| R1, R18, R19 | 3 | 1M | Resistor |  |  | Tayda |  |
| R2, R56–58 | 4 | 24k | Resistor |  |  | Tayda |  |
| R3, R16 | 2 | 5.6k | Resistor |  |  | Tayda |  |
| R4–6, R8, R10, R21, R26, R28 | 8 | 100k | Resistor |  |  | Tayda |  |
| R7 | 1 | 470k | Resistor |  |  | Tayda |  |
| R9, R12, R22, R27, R31, R44, R46, R62–64 | 10 | 10k | Resistor |  |  | Tayda |  |
| R11, R15 | 2 | 470R | Resistor |  |  | Tayda |  |
| R13 | 1 | 1.8k | Resistor |  |  | Tayda |  |
| R14 | 1 | 1.5M | Resistor |  |  | Tayda |  |
| R17, R20, R29 | 3 | 47k | Resistor |  |  | Tayda |  |
| R23, R24, R65–67 | 5 | 1k | Resistor |  |  | Tayda |  |
| R25 | 1 | 240k | Resistor |  |  | Tayda |  |
| R30, R49, R50 | 3 | 20k | Resistor |  |  | Tayda |  |
| R32–41 | 10 | 10k 1/8W | Resistor |  |  | Tayda |  |
| R42 | 1 | 30k | Resistor |  |  | Tayda |  |
| R43, R48 | 2 | 2.2k | Resistor |  |  | Tayda |  |
| R45 | 1 | 390R | Resistor |  |  | Tayda |  |
| R47, R52, R54, R55 | 4 | 18k | Resistor |  |  | Tayda |  |
| R51 | 1 | 2k | Resistor |  |  | Tayda |  |
| R53 | 1 | 33k | Resistor |  |  | Tayda |  |
| R59–61 | 3 | 9.1k | Resistor |  |  | Tayda |  |
| RV1, RV3, RV4, RV7, RV8 | 5 | 100k | Board mount linear potentiometer |  |  | Tayda | A-1848 or A-5513 |
| RV2, RV5, RV6 | 3 | 10k | 3296W trimmer potentiometer |  |  | Tayda | A-586 |
| RV9–11 | 3 | 50k | 3296W trimmer potentiometer |  |  | Tayda | A-598 |
| RV12 | 1 | 5k | 3296W trimmer potentiometer |  |  | Tayda | A-597 |
| SW1 | 1 | SW_Rotary12 | rotary switch with 12 positions |  |  | Tayda | A-1893 |
| SW2 | 1 | SW_DP3T | Switch, three position, dual pole triple throw, 3 position switch, SP3T | C&K | OS203012MU5QP1‎ | Digi-Key | CKN9554-ND |
| TP1, TP7 | 2 | +12V | test point |  |  |  |  |
| TP3, TP8 | 2 | -12V | test point |  |  |  |  |
| TP5 | 1 | GND | test point |  |  |  |  |
| TP9 | 1 | GND | test point |  |  |  |  |
| U1, U7 | 2 | TL074 | Quad operational amplifier, DIP-14 |  |  | Tayda | A-1138 |
| U2 | 1 | TL072 | Dual operational amplifier, DIP-8 |  |  | Tayda | A-037 |
| U3 | 1 | AS3340A | VCO |  |  | CabinTech Global |  |
| U4, U5 | 2 | LM4040BIZ-5 | 5.000V Precision Micropower Shunt Voltage Reference, TO-92 |  |  | Digi-Key | LM4040BIZ-5/NOPB-ND |
| U6 | 1 | L79L05_TO92 | Negative 100mA -30V Linear Regulator, Fixed Output -5V, TO-92 |  |  | Tayda | A-4676 |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|5|R23, R24, R65–67|
|10k|10|R9, R12, R22, R27, R31, R44, R46, R62–64|
|100k|8|R4–6, R8, R10, R21, R26, R28|
|1M|3|R1, R18, R19|
|1.5M|1|R14|
|1.8k|1|R13|
|18k|4|R47, R52, R54, R55|
|2k|1|R51|
|20k|3|R30, R49, R50|
|2.2k|2|R43, R48|
|24k|4|R2, R56–58|
|240k|1|R25|
|30k|1|R42|
|33k|1|R53|
|390R|1|R45|
|470R|2|R11, R15|
|47k|3|R17, R20, R29|
|470k|1|R7|
|5.6k|2|R3, R16|
|9.1k|3|R59–61|
|10k 1/8W|10|R32–41|

