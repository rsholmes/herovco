# Hero VCO troubleshooting

For general advice see

[General advice for troubleshooting a module and asking for help](https://lookmumnocomputer.discourse.group/t/general-advice-for-troubleshooting-a-module-and-asking-for-help/2049)

For more particular advice about 3340 VCOs see

[Advice for troubleshooting 3340 based VCO](https://lookmumnocomputer.discourse.group/t/advice-for-troubleshooting-3340-based-vco/2077)

and for even more particular advice about the Hero VCO:

This is just a rough start at a troubleshooting guide. I intend to expand and update it as needed.

# If there is no output at all

* Check all the waveform outputs, not just one. (But if there's nothing on pulse, ramp, or triangle, no need to bother checking sine.)

* Try changing the RV6 (`Center freq`) trimmer to see if that does anything.

* Use a scope or an audio probe to check whether there are any waveforms present on the 3340 output pins (Pins 4, 8, 10.) If there are the problem is somewhere downstream of the 3340. If not there is probably some assembly problem around the 3340.

## If there is no signal at 3340 output pins

### Connection checks

* *With power off,* check the resistance from 3340 pin 13 to the +12 V test point, it should be 1.5M I think. (Maybe unplug the 3340 before checking this.)

* Resistance from pin 14 to ground test point should be 1.8k.

* *With power on,* voltage at pin 3 should be -5 V and at pin 16 should be +12 V. 

* On U1, voltage at pin 4 should be +12 V and pin 11 should be -12 V.

Check for bad connections at those pins and the connected components if any of the above tests fail.

* Check the summed CV going into the 3340. It would be the sum of U1 pin 8, connected to R21, and the wipers of trimmers RV5 (`Hi freq track`) and RV6, connected to R19 and R20. You can measure at those resistors but you must measure on the sides of them that are not connected to the 3340 and each other:

![](../Images/cvsum.png)

These should add up to something above about 5 V, probably, to get an audio frequency output.
